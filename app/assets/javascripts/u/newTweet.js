var url = $('base').attr('href')+"/u/search-user?"; 
$(document).ready(function(){
  $('#tweet').elastic();
  var cachequeryMentions = [], itemsMentions,
  searchmentions = $('#tweet').atwho({
    at: "@",
    callbacks: {
            remote_filter: function (query, render_view) {
                var thisVal = query,
                self = $(this);
                if( !self.data('active') && thisVal.length >= 2 ){
                    self.data('active', true);
                    $('.btn-twitter-submit').prop('disabled', false);                           
                    itemsMentions = cachequeryMentions[thisVal]
                    if(typeof itemsMentions == "object"){
                        render_view(itemsMentions);
                    }else
                    {                            
                        if (self.xhr) {
                            self.xhr.abort();
                        }
                        self.xhr = $.getJSON(url,{
                            q: thisVal
                        }, function(data) {
                            cachequeryMentions[thisVal] = data
                            render_view(data);
                        });
                    }                            
                    self.data('active', false);                            
                }                    
            }
        }
    });
});