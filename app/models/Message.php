<?php

use LaravelBook\Ardent\Ardent;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Message extends Ardent implements StaplerableInterface {
    use EloquentTrait;

	protected $fillable = ['content','sender','recipient','attachment'];
	public static $rules = array(
	    'recipient'             => 'required',
	    'content'             => 'required',
	  );

	public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('attachment', []);

        parent::__construct($attributes);
    }
}