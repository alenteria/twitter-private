<?php

class PrivateTweetController extends \BaseController {
	public function __construct(){
		$this->viewPath = 'u.private_tweet';
	}
	public function index()
	{	
		return Redirect::to('/');
	}
 
	public function create()
	{	
		return View::make($this->viewPath.'.create');
	}
	public function store()
	{
		$attachment = Input::file('attachment');
		$tweet = Input::get('tweet');
		$recipientsArr = [];
		$str = explode(" ",$tweet);

		foreach($str as $k=>$word){
		    if(substr($word,0,1)=="@"){
		        $recipientsArr[] = $word;
		    }
		}
		 
		$m = []; 
		$file = '';
		$first_message  = '';
		if(count($recipientsArr) <=0 )
			return Redirect::back()->withErrors('Error! blank tweet recipient and message.');

		foreach ($recipientsArr as $key => $recipient) {
			$m = new Message();

			$m->content = Input::get('content');
			$m->sender = UserHelper::current()->id;
 
			$m->recipient = trim($recipient);
			$m->slug = bin2hex(openssl_random_pseudo_bytes(4));
			$m->save();
			if(Input::hasFile('attachment')){
					$m->attachment = $file!='' ? $file : $attachment;
					$m->save();

					$file = public_path().'/'.$m->attachment_file_name;
					copy(public_path().'/'.$m->attachment->url(), $file);

			} 
			if($m->errors()->all()){
				return Redirect::back()->withErrors($m->errors()->all());
			}

			$url = URL::to($m->slug);
			$tweet_text = $tweet;
			foreach ($recipientsArr as $key => $r) {
				if($recipient != $r)
			 		$tweet_text = str_replace($r, '', $tweet_text);
			 }
			$status = $tweet_text.' '.$url;
			TwitterApi::postTweet(array('status' => $status, 'format' => 'json'));
		}
 		return Redirect::back()->with('message', "The message was sent successfully");
	}
	public function show($slug)
	{	
		
		$oAuth = Session::get('access_token');
		if(!isset($oAuth['oauth_token'])){
			Session::put('intented_route', Request::url());
			$t_screen_name = 'blank_';
			Session::put('first_login', true);
		}else{
			$t_screen_name = UserHelper::current()->t_screen_name;
		}
		$message = Message::where('slug', $slug)->first();
		if(!$message)
			return View::make($this->viewPath.'.tweet_not_found');
		else{
			if(isset($message->recipient) && $message->recipient == $t_screen_name)
				return View::make($this->viewPath.'.tweet')->with('message', $message);
			else{
				if(!Session::has('intented_route')&&Session::has('first_login')){
					Session::forget('first_login');
					return Redirect::to('/');
				}

				$recipient = UserHelper::getUser(['screen_name'=>$message->recipient]);
				return View::make($this->viewPath.'.tweet_private')->with(['message'=>$message, 'recipient'=>$recipient]);
			}
		}

	} 
	public function edit($id)
	{
		//
	}
	public function update($id)
	{
		//
	}
	public function destroy($id)
	{
		ignore_user_abort(true);
		set_time_limit(0);

		Message::find($id)->delete();
		return Response::json(['status'=> 'ok']);	
	}

}