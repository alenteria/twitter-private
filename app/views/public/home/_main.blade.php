<div class="clearfix"><br/><br/></div>
<!-- MAIN SECTION -->
<section class ="main text-center">
  <div class="container responsive">
    <div class="row logo">
      <img src="assets/logo.png" alt="Private Tweets"><h1>Private Tweets</h1>
    </div>

    <div class="row">
      <p>Tweet private message that can only be viewed by the people you mentioned.</p>
    </div>
    <div class="clearfix"><br/><br/></div>
    <div class="row">
    @if(Session::has('access_token'))
      <a href="u/private-tweet/create" class="btn btn-lg btn-twitter-login" ><i class="fa fa-twitter">&nbsp;</i>New Private Tweet</a>
    @else
      <a href="/twitter-redirect" class="btn btn-lg btn-twitter-login" ><i class="fa fa-twitter">&nbsp;</i>Log In With Twitter</a>
    @endif
    </div>
    <div class="clearfix"><br/><br/></div>
    <div class="row">
      <p>
        <a href="/privacy" title="Privacy Policy">Privacy Policy</a>
      </p>
    </div>
  </div>
</section>
