@extends('base')
@section('head')
	@section('title')
	      Private Tweets | My Account 
	@stop
	<link rel="icon" href="assets/logo.png" type="image/x-icon">
	{{stylesheet_link_tag('u')}}
@stop
@section('body')
	<div class="wrapper u-wrapper">
	@yield('main_content')
	</div>
@stop
@section('scripts')
	{{javascript_include_tag('u')}}
	@yield('specific_scripts')
@stop
