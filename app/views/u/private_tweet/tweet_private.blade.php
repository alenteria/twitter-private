@extends('u.index')
@section('title')
  Private Tweets | Private Tweets
@stop
@section('main_content')  
<!-- MAIN SECTION -->
@include('u.layouts._top')
<section class ="main text-center">
  <div class="container responsive">
    <div class="clearfix"><br/><br/></div>
     
    <div class="row">
      <label>This message is private for this account</label>
    </div>
    
    <div class="row">
      <div class="inline">
        <img class="avatar" src="{{$recipient['profile_image_url']}}" alt="{{$message->recipient}}" style="border-radius: 100%; border: 1px solid #EBDCDC;">
      </div>
      <div class="inline">
        <label><a href="https://twitter.com/search?q={{$message->recipient}}">{{$message->recipient}}</a></label>
      </div>
    </div>
    <div class="clearfix"><br/><br/></div>
  </div>
</section>
@include('u.layouts._footer')

@stop
@section('specific_scripts')
@stop
