@extends('u.index')
@section('title')
  Private Tweets | My Private Tweets
@stop
@section('main_content')  
<!-- MAIN SECTION -->
@include('u.layouts._top')
<section class ="main text-center">
  <div class="container responsive">
    <div class="clearfix"><br/><br/></div>    
    <div class="row">
      <center>
        <div class="show-tweet">
          <div class="pull-left tweet-label">Message</div>
          <div class="clearfix"><br/></div>
          {{$message->content}}
          @if($message->attachment_file_name)
          <div class="clearfix"><br/><br/></div>
          <div class="tweet-attachment">
            <a onclick="window.open('{{URL::to($message->attachment->url())}}', '_blank')" style="cursor:pointer"><i class="fa fa-download">&nbsp;</i>{{$message->attachment_file_name}}</a>
          </div>
          @endif
        </div>
      </center>
    </div>
    <div class="clearfix"><br/><br/></div>
  </div>
</section>

@stop
@section('specific_scripts')
  <script type="text/javascript">
  // $(window).unload(function() {
  //   $.post("{{URL::to('del/'.$message->id)}}");
  // });
window.onbeforeunload = function() {
    $.post("{{URL::to('del/'.$message->id)}}");
}
  </script>
@stop
