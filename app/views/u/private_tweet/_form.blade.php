@if (Session::has('message'))
   <div class="alert alert-success form-alerts">{{ Session::get('message') }}</div>
@elseif(Session::has('errors'))
   	@foreach(Session::get('errors')->all() as $error)
	   <div class="alert alert-danger form-alerts">
   		{{$error}}
   		</div>
   	@endforeach
@endif

<section class="form">
	<?= Form::open(['url' => action('PrivateTweetController@store'), 'method' => 'POST', 'files' => true]) ?>
		<div class="container responsive note-con">
		     <div class="clearfix"><br/></div>
		     <div class="recipients-editor">
		     	<textarea placeholder="@Username this Private message I hope to answer them." id="tweet" name="tweet" rows="1" maxlength="119"></textarea>
		     </div>
		      <div class="clearfix"></div>
			<textarea placehoder="Private message here..." required class="input-block-level form-control" id="summernote" name="content" rows="18">
			</textarea>
			<i class="fa fa-download">&nbsp;</i>
			<input type="file" name="attachment" class="inline" title="attachment">
			<center>
		      <button type="submit" disabled class="btn btn-lg btn-twitter-submit" ><i class="fa fa-twitter">&nbsp;</i>Send Private Message</button>		
			</center>
		</div>
	</form>
</section>